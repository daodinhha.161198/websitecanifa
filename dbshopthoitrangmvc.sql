-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th8 10, 2022 lúc 05:52 AM
-- Phiên bản máy phục vụ: 10.4.24-MariaDB
-- Phiên bản PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `dbshopthoitrangmvc`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'Tên danh mục',
  `type` tinyint(3) DEFAULT 0 COMMENT 'Loại danh mục: 0 - Product, 1 - News',
  `avatar` varchar(255) DEFAULT NULL COMMENT 'Tên file ảnh danh mục',
  `description` text DEFAULT NULL COMMENT 'Mô tả chi tiết cho danh mục',
  `status` tinyint(3) DEFAULT 0 COMMENT 'Trạng thái danh mục: 0 - Inactive, 1 - Active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'Ngày tạo danh mục',
  `updated_at` datetime DEFAULT NULL COMMENT 'Ngày cập nhật cuối'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `name`, `type`, `avatar`, `description`, `status`, `created_at`, `updated_at`) VALUES
(3, 'ÁO PLO NAM CAO CẤP', 0, 'ap-polo-01.jpg', 'Thiết kế trẻ chung năng động thu hút mọi ánh nhìn', 0, '2022-08-08 18:47:41', NULL),
(4, 'Quần Jean Nam', 0, '', 'Mang đến sự trẻ chung năng động', 1, '2022-08-09 02:54:00', '2022-08-09 13:04:38'),
(5, 'ÁO SƠ MI NAM', 0, '', 'Cotton thoáng mát', 1, '2022-08-09 02:55:00', '2022-08-09 13:04:46'),
(6, 'ÁO THUN NỮ', 0, '', '', 1, '2022-08-09 05:16:18', '2022-08-09 13:04:50');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL COMMENT 'Id của danh mục mà tin tức thuộc về, là khóa ngoại liên kết với bảng categories',
  `name` varchar(255) NOT NULL COMMENT 'Tiêu đề tin tức',
  `summary` varchar(255) DEFAULT NULL COMMENT 'Mô tả ngắn cho tin tức',
  `avatar` varchar(255) DEFAULT NULL COMMENT 'Tên file ảnh tin tức',
  `content` text DEFAULT NULL COMMENT 'Mô tả chi tiết cho sản phẩm',
  `status` tinyint(3) DEFAULT 0 COMMENT 'Trạng thái danh mục: 0 - Inactive, 1 - Active',
  `seo_title` varchar(255) DEFAULT NULL COMMENT 'Từ khóa seo cho title',
  `seo_description` varchar(255) DEFAULT NULL COMMENT 'Từ khóa seo cho phần mô tả',
  `seo_keywords` varchar(255) DEFAULT NULL COMMENT 'Các từ khóa seo',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'Ngày tạo',
  `updated_at` datetime DEFAULT NULL COMMENT 'Ngày cập nhật cuối'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL COMMENT 'Id của user trong trường hợp đã login và đặt hàng, là khóa ngoại liên kết với bảng users',
  `fullname` varchar(255) DEFAULT NULL COMMENT 'Tên khách hàng',
  `address` varchar(255) DEFAULT NULL COMMENT 'Địa chỉ khách hàng',
  `mobile` int(11) DEFAULT NULL COMMENT 'SĐT khách hàng',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email khách hàng',
  `note` text DEFAULT NULL COMMENT 'Ghi chú từ khách hàng',
  `price_total` int(11) DEFAULT NULL COMMENT 'Tổng giá trị đơn hàng',
  `payment_status` tinyint(2) DEFAULT NULL COMMENT 'Trạng thái đơn hàng: 0 - Chưa thành toán, 1 - Đã thành toán',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'Ngày tạo đơn',
  `updated_at` datetime DEFAULT NULL COMMENT 'Ngày cập nhật cuối'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `fullname`, `address`, `mobile`, `email`, `note`, `price_total`, `payment_status`, `created_at`, `updated_at`) VALUES
(1, NULL, '', '', 0, '', '', 23344334, 0, '2022-07-20 17:01:29', NULL),
(2, NULL, 'Nguyễn Thu Thảo', 'hhjhjhjhjh', 375086050, 'anvy2830@gmail.com', 'jj', 1776000, 0, '2022-07-21 02:42:56', NULL),
(3, NULL, '', '', 0, '', '', 200000, 0, '2022-08-09 06:05:57', NULL),
(4, NULL, 'ĐÀO ĐÌNH HÀ', 'âsas', 242563214, 'nguyna@gmail.com', 'ấdadad', 200000, 0, '2022-08-09 19:45:12', NULL),
(5, NULL, 'dsdsdsds', 'sdsdsdsd', 122455544, 'dao@123gmail.com', '444', 200000, 0, '2022-08-09 19:45:38', NULL),
(6, NULL, '', '', 0, '', '', 2099020, 0, '2022-08-09 19:52:31', NULL),
(7, NULL, '', '', 0, '', '', 2099020, 0, '2022-08-09 19:57:41', NULL),
(8, NULL, 'đào đình hà', '79- hồ tùng mậu', 981236547, 'daodinhha.@gmail.com', 'shop ship hàng luôn cho em ạ', 1000000, 0, '2022-08-10 03:22:22', NULL),
(9, NULL, '', '', 0, '', '', 1000000, 0, '2022-08-10 03:44:30', NULL),
(10, NULL, '', '', 0, '', '', 1000000, 0, '2022-08-10 03:44:42', NULL),
(11, NULL, '', '', 0, '', '', 1000000, 0, '2022-08-10 03:45:32', NULL),
(12, NULL, 'nguyen van b', 'đại học thuong mai', 2147483647, 'nguynvana@gmail.com', 'ship ', 1400000, 0, '2022-08-10 03:46:44', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_details`
--

CREATE TABLE `order_details` (
  `order_id` int(11) DEFAULT NULL COMMENT 'Id của order tương ứng, là khóa ngoại liên kết với bảng orders',
  `product_name` varchar(150) DEFAULT NULL COMMENT 'Tên sp tại thời điểm đặt hàng',
  `product_price` int(11) DEFAULT NULL COMMENT 'Giá sản phẩm tương ứng tại thời điểm đặt hàng',
  `quantity` int(11) DEFAULT NULL COMMENT 'Số lượng sản phẩm tương ứng tại thời điểm đặt hàng'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `order_details`
--

INSERT INTO `order_details` (`order_id`, `product_name`, `product_price`, `quantity`) VALUES
(1, 'oppo', 23344334, 1),
(2, 'Zero Velvet Tint', 319000, 4),
(2, 'Zero Cushion', 500000, 1),
(3, 'ÁO POLO', 200000, 1),
(4, 'ÁO POLO', 200000, 1),
(5, 'ÁO POLO', 200000, 1),
(6, 'ÁO POLO', 200000, 2),
(6, 'Áo TankTop Nam', 200000, 8),
(6, 'quần jean nam', 20, 1),
(6, 'ÁO HOODIE NỮ', 99000, 1),
(7, 'ÁO POLO', 200000, 2),
(7, 'Áo TankTop Nam', 200000, 8),
(7, 'quần jean nam', 20, 1),
(7, 'ÁO HOODIE NỮ', 99000, 1),
(8, 'ÁO POLO', 200000, 5),
(9, 'ÁO POLO', 200000, 5),
(10, 'ÁO POLO', 200000, 5),
(11, 'ÁO POLO', 200000, 5),
(12, 'ÁO POLO', 200000, 7);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL COMMENT 'Id của danh mục mà sản phẩm thuộc về, là khóa ngoại liên kết với bảng categories',
  `title` varchar(255) DEFAULT NULL COMMENT 'Tên sản phẩm',
  `avatar` varchar(255) DEFAULT NULL COMMENT 'Tên file ảnh sản phẩm',
  `price` int(11) DEFAULT NULL COMMENT 'Giá sản phẩm',
  `amount` int(11) DEFAULT NULL COMMENT 'Số lượng sản phẩm trong kho',
  `summary` varchar(255) DEFAULT NULL COMMENT 'Mô tả ngắn cho sản phẩm',
  `content` text DEFAULT NULL COMMENT 'Mô tả chi tiết cho sản phẩm',
  `status` tinyint(3) DEFAULT 0 COMMENT 'Trạng thái danh mục: 0 - Inactive, 1 - Active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'Ngày tạo',
  `updated_at` datetime DEFAULT NULL COMMENT 'Ngày cập nhật cuối'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `category_id`, `title`, `avatar`, `price`, `amount`, `summary`, `content`, `status`, `created_at`, `updated_at`) VALUES
(8, 3, 'ÁO POLO', '1660013517-product-somi-nam-01.jpg', 200000, 10, 'Thiết kế thời trang năng đông', '', 1, '2022-08-09 02:51:57', NULL),
(9, 3, 'Áo TankTop Nam', '1660013773-product-ao-tee-nam.jpg', 200000, 20, 'Nguyên liệu hoàn toàn từ cotton 100%', '', 1, '2022-08-09 02:56:13', NULL),
(10, 3, 'quần jean nam', '1660023938-product-quan-jean-nam.jpg', 20, 23, '22222', '2222', 1, '2022-08-09 05:45:38', NULL),
(11, 3, 'ÁO HOODIE NỮ', '1660025300-product-ao-hoodie.jpg', 99000, 10, 'Thiết kế trẻ chung năng đông', 'sản phẩm được làm từ nguyên liệu nỉ bông, giúp bạn có 1 outfit ấp áp vào mùa đông', 1, '2022-08-09 06:08:20', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL COMMENT 'Tên đăng nhập',
  `password` varchar(255) DEFAULT NULL COMMENT 'Mật khẩu đăng nhập',
  `first_name` varchar(255) DEFAULT NULL COMMENT 'Fist name',
  `last_name` varchar(255) DEFAULT NULL COMMENT 'Last name',
  `phone` int(11) DEFAULT NULL COMMENT 'SĐT user',
  `address` varchar(255) DEFAULT NULL COMMENT 'Địa chỉ user',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email của user',
  `last_login` datetime DEFAULT NULL COMMENT 'Lần đăng nhập gần đây nhất',
  `status` tinyint(3) DEFAULT 0 COMMENT 'Trạng thái danh mục: 0 - Inactive, 1 - Active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'Ngày tạo',
  `updated_at` datetime DEFAULT NULL COMMENT 'Ngày cập nhật cuối'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`, `phone`, `address`, `email`, `last_login`, `status`, `created_at`, `updated_at`) VALUES
(12, 'admin', '$2y$10$kIG8wQNtq6iEUcuieb9ffO35O9wCAHgdqJxeSFDuDdwVFV0vTUDBu', '', '', 0, '', '', NULL, 0, '2022-08-08 16:56:19', NULL),
(13, 'daodinhha', '$2y$10$OJKMhjLev57BuGi/AFtC1e1TS9ncTWeDLHwtvwf5vV4SvFoDm.DZ6', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-08-09 06:02:03', NULL),
(14, 'nguyena', '$2y$10$H.MVvhniMlAjWhhFegX1Peo2enUbphstLlM0.P/J3ufmFZan2jAM6', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-08-09 06:14:07', NULL),
(15, '12345', '$2y$10$OtcbAtAsXStzpo8qpRbPRex3Ynx/PTCzIMqG.F.kG1POOmXkPoXka', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-08-09 06:20:05', NULL),
(16, '1234', '$2y$10$nEjq51wHHHqN.VtByIcCrefWtJcm9tZACX6VQ80iTc3jxqa/8xUtC', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-08-09 06:23:20', NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Chỉ mục cho bảng `order_details`
--
ALTER TABLE `order_details`
  ADD KEY `order_id` (`order_id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Các ràng buộc cho bảng `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Các ràng buộc cho bảng `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);

--
-- Các ràng buộc cho bảng `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
