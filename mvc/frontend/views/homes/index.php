<?php
require_once 'helpers/Helper.php';
?>
<!--    Hiểm thị sản phẩm-->
<div class="product-wrap">
    <div class="product container">
      <?php if (!empty($products)): ?>
          <h1 class="post-list-title">
              <a href="danh-sach-san-pham.html" class="link-category-item">Sản phẩm mới nhất</a>
          </h1>
          <div class="link-secondary-wrap row">
            <?php foreach ($products AS $product):
              $slug = Helper::getSlug($product['title']);
              $product_link = "san-pham/$slug/" . $product['id'] . ".html";
              $product_cart_add = "them-vao-gio-hang/" . $product['id'] . ".html";
              ?>
                <div class="service-link col-md-3 col-sm-6 col-xs-12">
                    <a href="<?php echo $product_link; ?>">
                        <img class="secondary-img img-responsive" title="<?php echo $product['title'] ?>"
                             src="../backend/assets/uploads/<?php echo $product['avatar'] ?>"
                             alt="<?php echo $product['title'] ?>"/>
                        <span class="shop-title">
                        <?php echo $product['title'] ?>
                    </span>
                    </a>
                    <span class="shop-price">
                            <?php echo number_format($product['price']) ?>
                </span>

                    <span class="add-to-cart" data-id="<?php echo $product['id']; ?>">
                        <a href="#" style="color: inherit">Thêm vào giỏ</a>
                    </span>
                </div>
            <?php endforeach; ?>
          </div>
      <?php endif; ?>
    </div>
</div>
<!--    END PRODUCT-->
<!--NEWS-->
<div class="news-wrap">
    <div class="news container">
        <h1 class="post-list-title">
            <a href="/news.html" class="link-category-item">Tin mới nhất</a>
        </h1>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12 category-two-item">
                <a href="news_detail.html" class="two-item-link-heading">
                    <span class="new-image-content">
                        <img src="assets/images/news.jpg"
                             title="BỘ SƯU TẬP THU ĐÔNG NĂM 2022"
                             alt="BỘ SƯU TẬP THU ĐÔNG NĂM 2022"
                             class="post-image-avatar img-responsive">
                    </span>
                </a>
                <div class="news-content-wrap">
                    <h3 class="category-heading timeline-post-title">
                        <a href="#">
                            BỘ SƯU TẬP THU ĐÔNG 2022 </a>
                    </h3>
                    <div class="news-description">
                        Trong bộ sưu tập, giám đốc sáng tạo Virginie Viard đã sử dụng đầy đủ các sắc
                        màu từ hồng, tím, xanh lá cây cho đến những tông màu mới đậm được pha
                        những trộn lấy cảm hứng từ những chuyến đi dạo mùa đông ở vùng nông thôn và
                        những gam màu của một buổi sáng mù sương.
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12 category-two-item">
                <a href="news_detail.html" class="two-item-link-heading">
                    <span class="new-image-content">
                        <img src="assets/images/news.jpg"
                             title="BỘ SƯU TẬP THU ĐÔNG NĂM 2022"
                             alt="BỘ SƯU TẬP THU ĐÔNG NĂM 2022"
                             class="post-image-avatar img-responsive">
                    </span>
                </a>
                <div class="news-content-wrap">
                    <h3 class="category-heading timeline-post-title">
                        <a href="#">
                            BỘ SƯU TẬP THU ĐÔNG 2022 </a>
                    </h3>
                    <div class="news-description">
                        Trong bộ sưu tập, giám đốc sáng tạo Virginie Viard đã sử dụng đầy đủ các sắc
                        màu từ hồng, tím, xanh lá cây cho đến những tông màu mới đậm được pha
                        những trộn lấy cảm hứng từ những chuyến đi dạo mùa đông ở vùng nông thôn và
                        những gam màu của một buổi sáng mù sương.
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 category-two-item">
                <a href="news_detail.html" class="two-item-link-heading">
                    <span class="new-image-content">
                        <img src="assets/images/news.jpg"
                             title="BỘ SƯU TẬP THU ĐÔNG NĂM 2022"
                             alt="BỘ SƯU TẬP THU ĐÔNG NĂM 2022"
                             class="post-image-avatar img-responsive">
                    </span>
                </a>
                <div class="news-content-wrap">
                    <h3 class="category-heading timeline-post-title">
                        <a href="#">
                            BỘ SƯU TẬP THU ĐÔNG 2022 </a>
                    </h3>
                    <div class="news-description">
                        Trong bộ sưu tập, giám đốc sáng tạo Virginie Viard đã sử dụng đầy đủ các sắc
                        màu từ hồng, tím, xanh lá cây cho đến những tông màu mới đậm được pha
                        những trộn lấy cảm hứng từ những chuyến đi dạo mùa đông ở vùng nông thôn và
                        những gam màu của một buổi sáng mù sương.
                    </div>
                </div>
            </div>
            
            
        </div>
    </div>
</div>
<!--END NEWS-->

